import pytest
from fastapi.testclient import TestClient
from main import app

client = TestClient(app)


@pytest.mark.parametrize("endpoint", ['authors', 'posts', 'authors/1', 'posts/1'])
def test_read_multiple_items(endpoint):
    response = client.get(f"/{endpoint}")
    assert response.status_code == 200
    assert response.url == f'http://testserver/{endpoint}'
