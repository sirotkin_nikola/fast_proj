## Установить необходимые системные зависимости для psycopg2:
- ``sudo apt-get install libpq-dev python3-dev``

## Установить psycopg2:
- ``pip install psycopg2``

## Настройка и запуск миграций:
- ``apt install alembic``
- ``alembic init alembic``
- ### В файле alembic.ini заменить значения:
    ``sqlalchemy.url = postgresql+psycopg2://fast_api:fast_api@localhost/fast_api``
- ### Создать миграции:
    ``alembic revision --autogenerate -m "create tables"``
- ### Применить миграции:
    ``alembic upgrade head``
- ### Отмена последней миграции
    ``alembic downgrade -1``

## Установить uvicorn:
``apt install uvicorn``

## Запуск сервера:
- ``uvicorn main:app --reload``
## Установить :
- ``pip install "python-jose[cryptography]"``
- ``pip install "passlib[bcrypt]"``

## На эндпоинт /register запрос отправлять в виде:
{
'username': 'test_username',
'password': 'test_password',
'email': 'test@mail.ru'
}

## На эндпоинт /token запрос отправлять в виде:
x-www-form-urlencoded:
  - username
  - password
  - 
## Эндпооинт подписка на канал автора:
```http://127.0.0.1:8000/имя_автора/токен_доступа_в_формате(eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiJUZXN0IiwiZXhwIjoxNzA4NzE1MzExfQ.M9r47Q8oksTLi2HsQzxyvK2kArwRfhaKXwyg_DFD0rs)```
При создании нового поста авторомЁ на котрого подписан,
присылается уведомление о его создании всем подписанным на автора

## Эндпоинт /users доступен только при наличии токена.

## Перед запуском сервера необходимо запустить Celery:
``celery -A tasks worker --loglevel=info``

## Загрузка изображения:
form-data -----> ключ - file, значение - поле для файла
                      - post_id        - id поста
Headers -------> ключ - Authorization, значение - токен
