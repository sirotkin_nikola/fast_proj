from fastapi import FastAPI
from fastapi.responses import HTMLResponse
from routers.post_route import postroute
from routers.author_route import authorroute
from routers.user_route import user_registerroute
from routers.security_route import securityroute
from routers.image_upload_route import imageroute
from routers.websocket_route import socketroute
import json
import asyncio
from tasks import celery_app
from fastapi_cache import FastAPICache
from fastapi_cache.backends.redis import RedisBackend
from redis import asyncio as aioredis

app = FastAPI()

app.include_router(postroute)
app.include_router(authorroute)
app.include_router(user_registerroute)
app.include_router(securityroute)
app.include_router(imageroute)
app.include_router(socketroute)


@app.on_event("startup")
async def startup():
    redis = aioredis.from_url("redis://localhost", encoding="utf8", decode_responses=True)
    FastAPICache.init(RedisBackend(redis), prefix="api:cache")


@app.get("/url")
async def get_all_urls():
    url_list = [{route.path: route.name} for route in app.routes]
    celery_app.send_task('tasks.test_cfunction', ('celery', 'complete'))
    return url_list
