from pydantic import BaseModel, ValidationError
from pydantic.fields import Field
from datetime import datetime


class AuthorCreate(BaseModel):
    name: str
    date_of_birth: datetime
    country: str


try:
    AuthorCreate()
except ValidationError as exc:
    print(repr(exc.errors()[0]['type']))
