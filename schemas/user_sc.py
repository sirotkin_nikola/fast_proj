from pydantic import BaseModel, ValidationError
from pydantic.fields import Field


class UserCreate(BaseModel):
    username: str
    email: str
    password: str


try:
    UserCreate()
except ValidationError as exc:
    print(repr(exc.errors()[0]['type']))
