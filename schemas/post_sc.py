from pydantic import BaseModel, ValidationError
from pydantic.fields import Field


class PostCreate(BaseModel):
    title: str
    text: str
    author_id: int = Field()


try:
    PostCreate()
except ValidationError as exc:
    print(repr(exc.errors()[0]['type']))
