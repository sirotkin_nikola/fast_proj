from sqlalchemy.orm import Session
from models.tokens import Token
import asyncio


async def create_token(db: Session, username_tok: str, user):
    db_tok = Token(tok=username_tok)
    user.token = db_tok
    db.add(db_tok)
    db.commit()
    db.refresh(db_tok)
    return db_tok
