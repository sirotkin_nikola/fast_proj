from sqlalchemy.orm import Session, joinedload
from models.post import Post
from dependencies.cached_obj import cache
from cachetools import cached
import asyncio


async def create_post(db: Session, title: str, text: str, author_id: int):
    db_post = Post(title=title, text=text, author_id=author_id)
    db.add(db_post)
    db.commit()
    db.refresh(db_post)
    return db_post


@cached(cache)
async def all_posts(db: Session):
    post_all = db.query(Post).options(joinedload(Post.author))
    return post_all.all()


@cached(cache)
async def one_post(db: Session, id_post: int):
    post_one = db.query(Post).options(joinedload(Post.author)).filter(Post.id == id_post).first()
    return post_one

