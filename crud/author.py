from sqlalchemy.orm import Session, joinedload
from models.author import Author
from datetime import datetime
import asyncio


async def create_author(db: Session, name: str, date_of_birth: datetime, country: str):
    db_author = Author(name=name, date_of_birth=date_of_birth, country=country)
    db.add(db_author)
    db.commit()
    db.refresh(db_author)
    return db_author


async def all_authors(db: Session):
    authors_all = db.query(Author).options(joinedload(Author.posts))
    return authors_all.all()


async def one_author(db: Session, id_author: int):
    author_one = db.query(Author).options(joinedload(Author.posts)).filter(Author.id == id_author).first()
    return author_one
