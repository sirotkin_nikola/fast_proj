import asyncio
from PIL import Image
import os
from models.post import Post
from fastapi.responses import JSONResponse


async def save_image(db, file, p_id: str):
    post_search = db.query(Post).filter(Post.id == p_id).first()
    if post_search: ## TODO сделать транзакцию что бы сохранять файл и записывать путь в Post model
        img = Image.open(file.file)
        img.save(f"images/{file.filename}")
        image_path = os.path.join(os.getcwd(), 'images', file.filename)
        post_search.image_path = image_path
        db.commit()
        db.refresh(post_search)
        return JSONResponse(content={"image load directory": image_path})
    else:
        return JSONResponse(content={"error": 'Post not found'})

