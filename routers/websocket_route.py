import json
from fastapi import FastAPI, WebSocket, APIRouter, Request, WebSocketDisconnect, Depends, HTTPException
from typing import List
from fastapi.responses import HTMLResponse
from fastapi.templating import Jinja2Templates
from database.connection import get_db
from dependencies.websocket_manager import manager
from dependencies.change_author import change_author_for_name
from sqlalchemy.orm import Session, joinedload
from models import *
import asyncio
from fastapi.security import OAuth2PasswordBearer
from models.tokens import Token

socketroute = APIRouter()

templates = Jinja2Templates(directory="templates")
oauth2_scheme = OAuth2PasswordBearer(tokenUrl="/token")

connected_clients = {}


# TODO как передать токен в браузере при get запросе что бы подключиться к серверу webhoock и проверить его с помощью
# TODO async def get(request: Request, author_name: str, token_connect: str = Depends(oauth2_scheme)):

@socketroute.get("/{author_name}/{token_connect}")
async def get(request: Request, author_name: str, token_connect: str):
    context = {"author_name": author_name, "token_connect": token_connect}
    return templates.TemplateResponse('html_template.html', {'request': request, **context})


@socketroute.websocket("/ws/{author_name}/{token_connect}")
async def websocket_endpoint(websocket: WebSocket, author_name: str, token_connect: str, db: Session = Depends(get_db)):
    token = db.query(Token).filter(Token.tok == token_connect).first()
    author_id = await change_author_for_name(author_name, db)
    await manager.connect(websocket)
    if not token or not author_id:
        await manager.send_personal_message(f'Неверный токен или автор', websocket)
        await websocket.close(code=1000, reason='incorrect input')
    else:
        if author_id not in connected_clients.keys():
            connected_clients[author_id] = [websocket]
        else:
            connected_clients[author_id].append(websocket)

        await manager.send_personal_message(f'Вы подписались на автора: {author_name}', websocket)
        try:
            while True:
                data = await websocket.receive_text()
                await manager.broadcast(data)

        except WebSocketDisconnect:
            manager.disconnect(websocket)
            connected_clients[author_id].remove(websocket)
