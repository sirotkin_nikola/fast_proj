from models import *
from schemas import user_sc
from sqlalchemy.orm import Session
from crud import user as crud_user
from crud import token as crud_token
from fastapi import APIRouter, Body, Depends, HTTPException
from database.connection import get_db
from dependencies.authenticate_user import authenticate_user
from dependencies.verify import verify_password
from dependencies.create_token import create_access_token, ACCESS_TOKEN_EXPIRE_MINUTES
from fastapi.security import OAuth2PasswordBearer, OAuth2PasswordRequestForm
from datetime import datetime, timedelta
import asyncio
from models.user import User

securityroute = APIRouter()
oauth2_scheme = OAuth2PasswordBearer(tokenUrl="/token")


@securityroute.post('/register', response_model=user_sc.UserCreate)
async def create_user(user: user_sc.UserCreate, db: Session = Depends(get_db)):
    return await crud_user.create_user(db=db, **user.dict())


@securityroute.post("/token")
async def get_token(db: Session = Depends(get_db), form_data: OAuth2PasswordRequestForm = Depends()):
    user = authenticate_user(db, form_data.username, form_data.password)
    if not user:
        raise HTTPException(status_code=401, detail="Invalid username or password")
    access_token_expires = timedelta(minutes=ACCESS_TOKEN_EXPIRE_MINUTES)
    access_token = create_access_token(
        data={"sub": user.username},
        expires_delta=access_token_expires
    )
    return await crud_token.create_token(db=db, username_tok=access_token, user=user)
