from fastapi import APIRouter, Body, Depends, HTTPException
from database.connection import get_db
from models import *
from schemas import post_sc
from sqlalchemy.orm import Session
from crud import post as crud_post
import asyncio
from fastapi_cache.decorator import cache


postroute = APIRouter()


@postroute.get('/posts')
@cache(expire=60)
async def get_posts(db: Session = Depends(get_db)):
    return await crud_post.all_posts(db=db)


@postroute.post('/posts', response_model=post_sc.PostCreate)
async def create_post(post: post_sc.PostCreate, db: Session = Depends(get_db)):
    return await crud_post.create_post(db=db, **post.dict())


@postroute.get('/posts/{post_id}')
@cache(expire=60)
async def get_posts(post_id: int, db: Session = Depends(get_db)):
    return await crud_post.one_post(id_post=post_id, db=db)
