from sqlalchemy.orm import Session
from crud import user as crud_user
from fastapi import APIRouter, Depends, HTTPException
from database.connection import get_db
from fastapi.security import OAuth2PasswordBearer
from models.tokens import Token
import asyncio
from fastapi_cache.decorator import cache

user_registerroute = APIRouter()
oauth2_scheme = OAuth2PasswordBearer(tokenUrl="/token")

ALLOWED_ROLES = ['admin']


@user_registerroute.get('/users')  # Доступ по токену, только пользователю 'admin'
@cache(expire=60)
async def get_users(db: Session = Depends(get_db), username_tok: str = Depends(oauth2_scheme)):
    input_token = db.query(Token).filter(Token.tok == username_tok).first()
    if not input_token:
        raise HTTPException(status_code=404, detail="404 Invalid token")
    elif input_token.user[0].role not in ALLOWED_ROLES:
        raise HTTPException(status_code=403, detail="403 Forbidden")
    return await crud_user.all_users(db=db)
