from fastapi import File, UploadFile, APIRouter, Depends, HTTPException, Form
from fastapi.responses import JSONResponse
from crud.image_upload import save_image
from PIL import UnidentifiedImageError
from fastapi.security import OAuth2PasswordBearer
from database.connection import get_db
from sqlalchemy.orm import Session
from models.tokens import Token

imageroute = APIRouter()
oauth2_scheme = OAuth2PasswordBearer(tokenUrl="/token")


@imageroute.post("/api/images/upload")
async def upload_image(file: UploadFile = File(...), post_id: str = Form(...),
                       db: Session = Depends(get_db),
                       username_tok: str = Depends(oauth2_scheme)):
    if not db.query(Token).filter(Token.tok == username_tok).first():
        raise HTTPException(status_code=404, detail="Invalid token")
    try:
        return await save_image(db, file, post_id)
    except UnidentifiedImageError as error:
        return JSONResponse(content={"error": str(error)})
