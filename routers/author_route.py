from models import *
from schemas import author_sc
from sqlalchemy.orm import Session
from crud import post as crud_post
from crud import author as crud_author
from fastapi import APIRouter, Body, Depends, HTTPException
from database.connection import get_db
import asyncio
from fastapi_cache.decorator import cache

authorroute = APIRouter()


@authorroute.get('/authors')
@cache(expire=60)
async def get_authors(db: Session = Depends(get_db)):
    return await crud_author.all_authors(db=db)


@authorroute.post('/authors', response_model=author_sc.AuthorCreate)
async def create_author(author: author_sc.AuthorCreate, db: Session = Depends(get_db)):
    return await crud_author.create_author(db=db, **author.dict())


@authorroute.get('/authors/{author_id}')
@cache(expire=60)
async def get_author(author_id: int, db: Session = Depends(get_db)):
    return await crud_author.one_author(id_author=author_id, db=db)
