from celery import Celery

celery_app = Celery('tasks', broker='redis://localhost:6379/0', backend='redis://localhost:6379/0')


@celery_app.task
def test_cfunction(x, y):
    print(f'--------{x}-----{y}-------')
    return None
