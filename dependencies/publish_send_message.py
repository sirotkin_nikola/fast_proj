import asyncio


async def publish_message(author_id, con_clients):
    for client in con_clients[author_id]:
        await client.send_text('У автора появился новый пост')