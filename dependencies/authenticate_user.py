from fastapi import FastAPI, HTTPException, Depends
from fastapi.security import OAuth2PasswordBearer, OAuth2PasswordRequestForm
from passlib.context import CryptContext
from datetime import datetime, timedelta
from typing import Optional
from pydantic import BaseModel
from models.user import User
from dependencies.verify import verify_password
from sqlalchemy.orm import Session
from jose import JWTError, jwt
from fastapi.responses import Response

pwd_context = CryptContext(schemes=["bcrypt"], deprecated="auto")


def authenticate_user(db: Session, username: str, password: str) -> Optional[User]:
    db_user_change = db.query(User).filter(User.username == username).first()
    if db_user_change in db.query(User).all():
        if verify_password(password, db_user_change.password):
            return db_user_change
    return None
