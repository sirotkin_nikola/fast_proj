from models import *
from sqlalchemy.orm import joinedload


async def change_author_for_name(author_n, db):
    author_websock = db.query(Author).options(joinedload(Author.posts)).filter(Author.name == author_n).first()
    if author_websock:
        return author_websock.id
    return None