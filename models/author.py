from models.base import Base
from sqlalchemy import Column, Integer, String, DateTime, ForeignKey
from sqlalchemy.orm import relationship
from sqlalchemy.sql import func
from datetime import datetime


class Author(Base):
    __tablename__ = 'authors'
    id = Column(Integer, primary_key=True, index=True)
    name = Column(String, index=True)
    date_of_birth = Column(DateTime)
    country = Column(String)
    posts = relationship('Post', back_populates='author')

    def to_dict(self):
        return {c.name: str(getattr(self, c.name))
                if isinstance(getattr(self, c.name), datetime)
                else getattr(self, c.name) for c in self.__table__.columns}
