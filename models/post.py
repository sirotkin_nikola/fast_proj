import asyncio
from models.base import Base
from sqlalchemy import Column, Integer, String, DateTime, ForeignKey, event
from sqlalchemy.orm import relationship
from sqlalchemy.sql import func
from datetime import datetime
from dependencies.publish_send_message import publish_message
from routers.websocket_route import connected_clients


class Post(Base):
    __tablename__ = 'posts'
    id = Column(Integer, primary_key=True, index=True)
    title = Column(String, index=True)
    text = Column(String)
    author_id = Column(Integer, ForeignKey('authors.id'))
    created_at = Column(DateTime, default=func.now())
    image_path = Column(String, default=None)
    author = relationship('Author', back_populates='posts')

    def to_dict(self):
        return {c.name: str(getattr(self, c.name))
                if isinstance(getattr(self, c.name), datetime)
                else getattr(self, c.name) for c in self.__table__.columns}


@event.listens_for(Post, 'after_insert')
def receive_after_insert(mapper, connection, target):
    asyncio.create_task(publish_message(target.author_id, connected_clients))

