from models.base import Base
from sqlalchemy import Column, Integer, String, DateTime, ForeignKey
from sqlalchemy.sql import func
from sqlalchemy.orm import relationship


class User(Base):
    __tablename__ = 'user'
    id = Column(Integer, primary_key=True, index=True)
    username = Column(String, index=True)
    email = Column(String)
    password = Column(String)
    registered_at = Column(DateTime, default=func.now())
    role = Column(String, default='user')
    user_token = Column(Integer, ForeignKey('tokens.id'), nullable=True, default=None)
    token = relationship('Token', back_populates='user')
